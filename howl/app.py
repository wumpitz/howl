import flask
from celery import Celery
from flask import Flask
from flask.ext import restful
from flask.ext.babel import Babel
from six import iterkeys

from werkzeug.exceptions import default_exceptions
from werkzeug.exceptions import HTTPException

from howl.core.bindings import load_bindings, load_devices
from howl.core.database import connection
from howl.core.devices import register, BaseDevice
from howl.core.documents import admin_app
from howl.core.resources.binding import BindingResource, BindingListResource
from howl.core.resources.device import DeviceResource
from howl.core.resources.item import ItemResource

celery = Celery(__name__)


def initalise_bindings(app):
    register(BaseDevice, with_admin=False)
    installed_bindings = app.config['INSTALLED_BINDINGS']
    load_bindings(app.config, installed_bindings)
    load_devices(installed_bindings)


def configure_api(app):
    api = restful.Api(app)
    api.add_resource(BindingListResource, '/bindings/')
    api.add_resource(BindingResource, '/bindings/<string:code>', endpoint='binding')
    api.add_resource(DeviceResource, '/devices/<string:id>', endpoint='device')
    api.add_resource(DeviceResource, '/devices/', endpoint='devices')
    api.add_resource(ItemResource, '/items/<string:id>', endpoint='item')
    api.add_resource(ItemResource, '/items/', endpoint='items')


def configure_app(app):
    app.config.from_pyfile('conf/settings.py')
    babel = Babel(app)
    connection.initialise(app)
    admin_app.init_app(app)
    celery.conf.update(app.config.get('CELERY'))


def create_app(env=None, **kwargs):
    """
    Creates a JSON-oriented Flask app.

    All error responses that you don't specifically
    manage yourself will have application/json content
    type, and will contain JSON like this (just an example):

    { "message": "405: Method Not Allowed" }
    """
    def make_json_error(ex):
        response = flask.jsonify(message=str(ex))
        response.status_code = (ex.code if isinstance(ex, HTTPException) else 500)
        return response

    app = Flask(__name__, **kwargs)

    for code in iterkeys(default_exceptions):
        app.error_handler_spec[None][code] = make_json_error

    configure_app(app)
    initalise_bindings(app)
    configure_api(app)
    return app
