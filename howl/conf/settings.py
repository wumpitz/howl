
HOMEMATIC = {
    'host': '192.168.1.48',
    #'host': 'localhost',
    'port': 2001,
    'secure': False,
    'use_mock': True
}

# MongoDB configuration
MONGODB_HOST = 'localhost'
MONGODB_PORT = 27017
MONGODB_DB = 'howl'

DEBUG = True

SECRET_KEY = 'hu65/&ZUHLIUzt76trgizGFZU%6rtgiugu6&Utugjlk'

CELERY = {
    'BROKER_URL': 'redis://localhost:6379',
    'CELERY_ACCEPT_CONTENT': ['json', 'msgpack', 'yaml'],
    'CELERY_TASK_SERIALIZER': 'json',
    'CELERY_TIMEZONE': 'Europe/London',
    'CELERY_ALWAYS_EAGER': True,
    'CELERY_EAGER_PROPAGATES_EXCEPTIONS': True
}

INSTALLED_BINDINGS = [
    'howl.bindings.homematic',
]
