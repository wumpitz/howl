from mongoengine import StringField, EmbeddedDocument, EmbeddedDocumentField, IntField
from howl.core.devices import BaseDevice, register


class HomematicFields(EmbeddedDocument):
    address = StringField(required=True)
    peer_id = IntField(required=True)


class HomematicDevice(BaseDevice):
    attributes = EmbeddedDocumentField(HomematicFields)

    @property
    def peer_id(self):
        return self.attributes.peer_id


register(HomematicDevice)
