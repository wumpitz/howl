from howl.core.items import ItemView
from flask import flash
from flask.ext.babel import gettext
from flask_admin.actions import action


class HomematicItemView(ItemView):
    @action('mock_update', 'Update values with fake data', 'Are you sure you want to update the values with fake data'
                                                           'for the selected items?')
    def action_mock_update(self, ids):
        query = self.model.objects(id__in=ids, readable=True)
        for item in query:
            pass
            # Do something
        flash(gettext('Triggered tasks for fetching the values for the items.'))
