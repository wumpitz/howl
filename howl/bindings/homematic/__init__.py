from howl.core.processes import library
from howl.core.bindings.gateway import BaseGateway
from .processes import MockProcess, CCUEventHandler, CCUServerProcess
from .items import fetch_item_value, set_item_value
from .client import ApiClient


class BindingGateway(BaseGateway):
    name = "Homematic"

    client = None

    def fetch_item_value(self, item):
        return fetch_item_value(item, self.client)

    def set_item_value(self, item, value):
        return set_item_value(item, value, self.client)

    def on_initialized(self):
        if self.conf.get('use_mock', False):
            self.conf.update(dict(host='localhost', port=8888, secure=False))
            library.register(MockProcess, host='localhost', port=8888)
        self.client = ApiClient(self.conf.get('host'),
                                self.conf.get('port'),
                                secure=self.conf.get('secure'))
        library.register(CCUServerProcess, host='0.0.0.0', port=9876, event_handler=CCUEventHandler)


gateway = BindingGateway
