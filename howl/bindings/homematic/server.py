from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler


# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)


# Register an instance; all the methods of the instance are
# published as XML-RPC methods
class HomematicRPCFunctions:
    def __init__(self, process):
        self.process = process

    def event(self, *args, **kwargs):
        print('EVENT', args, kwargs)
        self.process.send('Olala')
        return


def create_homematic_rpc_server(host, port, *args, **kwargs):
    # Create server
    server = SimpleXMLRPCServer((host, port), allow_none=True,
                                requestHandler=RequestHandler)
    server.register_introspection_functions()
    server.register_instance(HomematicRPCFunctions(*args, **kwargs))
    return server


if __name__ == '__main__':
    # Run the server's main loop
    server = create_homematic_rpc_server('localhost', 9876)
    server.serve_forever()
