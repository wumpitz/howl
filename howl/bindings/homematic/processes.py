from howl.core.processes import BaseProcess, BaseEventHandler
from .mock.ccu import create_ccu_mock_server
from .server import create_homematic_rpc_server


class CCUEventHandler(BaseEventHandler):
    def handle(self, value):
        print(value)


class CCUServerProcess(BaseProcess):
    """Process to serve as a XML RPC server to receive events from the CCU unit"""
    def run(self, host, port):
        server = create_homematic_rpc_server(host, port, self)
        server.serve_forever()


class MockProcess(BaseProcess):
    def run(self, host, port):
        server = create_ccu_mock_server(host, port)
        server.serve_forever()
