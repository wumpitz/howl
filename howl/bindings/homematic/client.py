from xmlrpc.client import ServerProxy


class ApiClient(object):

    def __init__(self, host, port, secure=False):
        self.secure = secure
        self.protocol = 'https' if self.secure else 'http'
        self.port = port
        self.host = host
        self.uri = '%s://%s:%s' % (self.protocol, self.host, self.port)
        self.proxy = ServerProxy(self.uri)

    def list_devices(self):
        """Return a list of devices

        :return: List of devices
        """
        return self.call('listDevices')

    def get_value(self, peer_id, channel, key):
        """Return the value of the device, specified by channel and key (parameterName)

        :param peer_id:
        :param channel:
        :param key:
        :return:
        """
        return self.call('getValue', peer_id, channel, key)

    def set_value(self, peer_id, channel, key, value):
        """Set the value of the device, specified by channel and key (parameterName)

        :param peer_id:
        :param channel:
        :param key:
        :param value:
        :return:
        """
        return self.call('setValue', peer_id, channel, key, value)

    def register_server(self, uri, name):
        """Register a XmlRPC-Server for callbacks

        :param uri: URI of the server
        :param name: Server name
        :return:
        """
        return self.call('init', uri, name)

    def call(self, method_name, *args, **kwargs):
        """Call the given method using the ServerProxy

        :param method_name: Method to be called
        :param args: Arguments passed through
        :param kwargs: Keyword arguments passed through
        """
        return getattr(self.proxy, method_name)(*args, **kwargs)


if __name__ == '__main__':
    client = ApiClient('192.168.1.48', 2001, secure=False)
    print(client.list_devices())
