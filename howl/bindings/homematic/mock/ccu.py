import random
from xmlrpc.server import SimpleXMLRPCServer
from xmlrpc.server import SimpleXMLRPCRequestHandler


# Restrict to a particular path.
class RequestHandler(SimpleXMLRPCRequestHandler):
    rpc_paths = ('/RPC2',)


class CCUMock(object):
    NAME_MAPPING = (
        ('TEMPERATURE', 14, 24, float),
    )

    def _get_mapping(self, name):
        for mapping in self.NAME_MAPPING:
            if name == mapping[0]:
                return mapping

    """Simple mock of the XMLRPC API of a CCU unit"""
    def getValue(self, id, channel, name):
        mapping = self._get_mapping(name)
        if mapping:
            return mapping[3](random.randrange(*mapping[1:3]))
        return 18.0


def create_ccu_mock_server(ip, port):
    # Create server
    server = SimpleXMLRPCServer((ip, port), allow_none=True,
                                requestHandler=RequestHandler)
    server.register_introspection_functions()
    server.register_instance(CCUMock())
    return server


if __name__ == '__main__':
    # Run the server's main loop
    server = create_ccu_mock_server('localhost', 2001)
    server.serve_forever()
