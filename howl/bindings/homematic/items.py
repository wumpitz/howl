from mongoengine import StringField
from howl.core.items import BaseItem, register


class HomematicItem(BaseItem):
    channel = StringField(required=True)
    key = StringField(required=True)


register(HomematicItem)


def fetch_item_value(item, api_client):
    """Get the current value of the item, using the xmlrpc client

    :param item: Item to get the value for
    :type item: HomematicItem
    :param api_client: Homematic API client
    :type api_client: howl.bindings.homematic.client.ApiClient
    :return: Real value
    """
    return api_client.get_value(item.device.peer_id, int(item.channel), item.key)


def set_item_value(item, value, api_client):
    """Get the current value of the item, using the xmlrpc client

    :param item: Item to get the value for
    :type item: HomematicItem
    :param value: Value to set
    :param api_client: Homematic API client
    :type api_client: howl.bindings.homematic.client.ApiClient
    """
    return api_client.set_value(item.device.peer_id, item.channel, item.key, value)
