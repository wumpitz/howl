from flask.ext.mongoengine import MongoEngine


class Connection(object):
    db = None

    def initialise(self, app):
        self.db = MongoEngine(app)


connection = Connection()
