from mongoengine import DoesNotExist
from flask.ext.restful import abort

from howl.core.bindings import get_binding, BindingNotFound
from howl.core.devices import BaseDevice
from howl.core.items import BaseItem


def get_binding_or_raise_404(code):
    try:
        binding = get_binding(code)
    except BindingNotFound as e:
        abort(404, message=e.message)
    else:
        return binding


def get_device_or_raise_404(id):
    try:
        device = BaseDevice.objects.get(id=id)
    except DoesNotExist as e:
        abort(404, message=e.message)
    else:
        return device


def get_item_or_raise_404(id):
    try:
        item = BaseItem.objects.get(id=id)
    except DoesNotExist as e:
        abort(404, message=e.message)
    else:
        return item
