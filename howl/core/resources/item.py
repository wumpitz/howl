from flask import request
from flask.ext.restful import abort

from howl.core.items import BaseItem, get_item_class_for_binding, exceptions
from . import Resource
from .shortcuts import get_item_or_raise_404, get_device_or_raise_404
from .marshaller import marshal, Marshaller


def resolve_item_path(path):
    return path.split('.')[-2:]


def find_item_class(binding, item_name):
    try:
        item_klass = get_item_class_for_binding(binding, item_name)
    except exceptions.ItemClassNotFound as e:
        abort(400, message=e.message)
    else:
        return item_klass


class ItemResource(Resource):
    """
    @TODO Some fields mustn't be set: _cls, id, binding_code, device_type
    """

    def get(self, id=None):
        if id:
            devices = get_item_or_raise_404(id)
        else:
            devices = BaseItem.objects
        return marshal(devices), 200

    def post(self):
        r_json = request.json
        binding, device_class = self._resolve_binding_and_class(r_json)
        doc = device_class(binding_code=binding)
        Marshaller(doc).loads(r_json)
        doc.save()
        return marshal(doc), 200

    def put(self, id):
        doc = get_item_or_raise_404(id)
        Marshaller(doc).loads(request.json)
        doc.save()
        return marshal(doc), 200

    def delete(self, id):
        device = get_item_or_raise_404(id)
        device.delete()
        return None, 200

    def _resolve_binding_and_class(self, r_json):
        def _extract_item_class(data):
            if 'item_class' not in data:
                abort(400, message='You have to provide an item class')
            return data.pop('item_class')
        device = get_device_or_raise_404(r_json['device_id'])
        binding, item_name = resolve_item_path(_extract_item_class(r_json))
        item_class = find_item_class(binding, item_name)
        return binding, item_class
