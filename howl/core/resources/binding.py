from flask.ext.restful import Resource, fields, marshal_with
from howl.core.bindings import bindings
from howl.core.resources.shortcuts import get_binding_or_raise_404


resource_fields = {
    'name':    fields.String,
    'code':    fields.String,
    'uri':     fields.Url('binding'),
    'devices': fields.Url('devices')
}


class BindingResource(Resource):
    @marshal_with(resource_fields)
    def get(self, code):
        binding = get_binding_or_raise_404(code)
        return binding, 200


class BindingListResource(Resource):
    @marshal_with(resource_fields)
    def get(self):
        return bindings, 200
