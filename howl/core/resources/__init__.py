from mongoengine import ValidationError
from flask.ext.restful import Resource as RestfulResource


class Resource(RestfulResource):
    """
    The Resource handles ValidationError exceptions
    """
    def dispatch_request(self, *args, **kwargs):
        try:
            return super(Resource, self).dispatch_request(*args, **kwargs)
        except ValidationError as e:
            errors = {}
            if e.field_name:
                errors[e.field_name] = unicode(e.message)
            if e.errors:
                errors.update({k: unicode(v) for k, v in e.errors.items()})
            return {"field-errors": errors}, 400

