from flask import request
from flask.ext.restful import abort

from . import Resource
from .marshaller import Marshaller, marshal
from .shortcuts import get_binding_or_raise_404, get_device_or_raise_404

from howl.core.devices import get_device_class_for_binding, DeviceClassNotFound, BaseDevice


def resolve_device_path(path):
    return path.split('.')[-2:]


def find_device_class(binding, device_name):
    try:
        device_klass = get_device_class_for_binding(binding, device_name)
    except DeviceClassNotFound as e:
        abort(400, message=e.message)
    else:
        return device_klass


class DeviceResource(Resource):
    """
    @TODO Some fields mustn't be set: _cls, id, binding_code, device_type
    """

    def get(self, id=None):
        if id:
            devices = get_device_or_raise_404(id)
        else:
            devices = BaseDevice.objects
        return marshal(devices), 200

    def post(self):
        r_json = request.json
        binding, device_class = self._resolve_binding_and_class(r_json)
        doc = device_class(binding_code=binding)
        Marshaller(doc).loads(r_json)
        doc.save()
        return marshal(doc), 200

    def put(self, id):
        doc = get_device_or_raise_404(id)
        Marshaller(doc).loads(request.json)
        doc.save()
        return marshal(doc), 200

    def delete(self, id):
        device = get_device_or_raise_404(id)
        device.delete()
        return None, 200

    def _resolve_binding_and_class(self, r_json):
        def _extract_device_type(data):
            if 'device_type' not in data:
                abort(400, message='You have to provide a device type')
            return data.pop('device_type')
        binding, device_name = resolve_device_path(_extract_device_type(r_json))
        device_class = find_device_class(binding, device_name)
        return binding, device_class
