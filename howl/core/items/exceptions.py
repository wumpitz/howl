class ItemNotFound(Exception):
    pass


class ItemClassNotFound(Exception):
    pass
