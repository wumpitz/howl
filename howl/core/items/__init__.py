import datetime

from mongoengine import Document, StringField, ReferenceField, CASCADE, DynamicField, EmbeddedDocument, DateTimeField, \
    ListField, EmbeddedDocumentField, BooleanField

from howl.core.documents import admin_app
from howl.core.items.admin import ItemView
from .exceptions import ItemClassNotFound

VALUE_TYPE_STRING = 'string'
VALUE_TYPE_INT = 'int'
VALUE_TYPE_FLOAT = 'float'

VALUE_TYPES = (
    VALUE_TYPE_STRING, VALUE_TYPE_INT, VALUE_TYPE_FLOAT
)


class PersistedValue(EmbeddedDocument):
    value = DynamicField(required=True)
    persisted_at = DateTimeField(required=True, default=datetime.datetime.now)

    def __repr__(self):
        return '<PersistedValue %s at %s>' % (self.value, self.persisted_at)


class BaseItem(Document):
    name = StringField(required=True)
    binding_code = StringField(required=True)
    device = ReferenceField('BaseDevice', required=True, reverse_delete_rule=CASCADE)
    value_type = StringField(required=True, choices=VALUE_TYPES)
    readable = BooleanField(default=True)
    writable = BooleanField(default=False)
    persist = BooleanField(default=False)

    value = DynamicField()
    updated_at = DateTimeField()
    persisted_values = ListField(EmbeddedDocumentField(PersistedValue), required=False)

    uri_endpoint = 'item'
    uri_marshal_field_name = 'uri'

    meta = {
        'allow_inheritance': True,
        'collection': 'items'
    }

    admin_view_class = ItemView

    def __repr__(self):
        return '<Item %s of %s/%s>' % (self.name, self.device.binding_code, self.device.name)


_registered_items = {}


def register(klass, with_admin=True):
    """
    Register a device class
    """
    binding_name = klass.__module__.split('.')[-2]
    if binding_name not in _registered_items:
        _registered_items[binding_name] = []
    _registered_items[binding_name].append(klass)
    if with_admin:
        admin_app.add_view(klass.admin_view_class(klass, category='Items'))


def get_items_for_binding(code):
    items = []
    item_classes = _registered_items[code]
    for klass in item_classes:
        d = list(getattr(klass, 'objects'))
        items += d
    return items


def get_item_class_for_binding(code, name):
    item_classes = _registered_items[code]
    for klass in item_classes:
        if klass.__name__ == name:
            return klass
    raise ItemClassNotFound('ItemClass "%s" for binding "%s" not found' % (name, code))
