from flask import flash
from flask.ext.admin.contrib.mongoengine import ModelView
from flask.ext.babel import gettext
from flask_admin.actions import action


class ItemView(ModelView):
    @action('fetch', 'Fetch values', 'Are you sure you want to fetch the values for the selected items?')
    def action_fetch_values(self, ids):
        from howl import tasks
        query = self.model.objects(id__in=ids, readable=True)
        for item in query:
            tasks.call_fetch_value(str(item.id))
        flash(gettext('Triggered tasks for fetching the values for the items.'))
