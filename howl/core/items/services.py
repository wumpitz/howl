import datetime

from howl.core.items import PersistedValue


def create_persisted_value(value, persisted_at=None):
    """Helper for creating a `PersistedValue` instance

    :param value: the value to persist
    :param persisted_at: an optional datetime
    :return: the created `PersistedValue`
    :rtype: how.core.items.PersistedValue
    """
    if not persisted_at:
        persisted_at = datetime.datetime.now()
    return PersistedValue(value=value, persisted_at=persisted_at)


def update_item(item, value):
    """Update an item with the given value. Creates `PersistedValue` if needed.

    :param item: item to update
    :type item: howl.core.items.BaseItem
    :param value: value to set
    :return: the item
    :rtype: howl.core.items.BaseItem
    """
    updated_at = datetime.datetime.now()
    if item.persist and value != item.value:
        item.persisted_values.append(create_persisted_value(value, item.updated_at))
    # Save the value and the current time
    item.value = value
    item.updated_at = updated_at
    item.save()
    return item