from flask import current_app


class BaseEventHandler(object):

    def __init__(self):
        self.app = current_app

    def handle(self, value):
        pass


class BaseProcess(object):

    def __init__(self, connection):
        self.con = connection

    def run(self, *args, **kwargs):
        pass

    def send(self, value):
        self.con.send(value)
