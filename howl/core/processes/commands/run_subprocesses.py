from flask.ext.script import Command

from howl.core.processes import library


class Executor(Command):
    """Starts all registered subprocesses"""

    def run(self):
        from flask import current_app
        current_app.logger.debug("Starting subprocesses...")
        library.run_all(current_app)
        library.wait()
        current_app.logger.debug("All processes up and running.")
