from multiprocessing import Process
from multiprocessing.connection import Pipe, wait


def run_process(target_class, read_con, *args, **kwargs):
    pcs = target_class(read_con)
    pcs.run(*args, **kwargs)


class RegisteredProcess(object):
    def __init__(self, target, if_true=None, name=None, event_handler=None, *args, **kwargs):
        self.target = target
        self.if_true = if_true
        self.name = name
        self.args = args
        self.kwargs = kwargs
        self.read_con = None
        self.process = None
        self.event_handler = event_handler

    def run(self, app):
        self.read_con, w_c = Pipe(duplex=False)
        self.kwargs['target_class'] = self.target
        self.kwargs['read_con'] = w_c
        self.process = Process(target=run_process, args=self.args, kwargs=self.kwargs)
        self.process.start()
        # We close the writable end of the pipe now to be sure that
        # p is the only process which owns a handle for it.  This
        # ensures that when p closes its handle for the writable end,
        # wait() will promptly report the readable end as being ready.
        w_c.close()
        return self.process

    def can_run(self, app):
        if self.if_true is None:
            return True
        return self.if_true(app)

    def handle_event(self, value):
        if not self.event_handler:
            return
        eh = self.event_handler()
        eh.handle(value)

    def __repr__(self):
        if self.name:
            return "Process '{0}' with target '{1}'".format(self.name, self.target.__name__)
        return "Process with target '{0}'".format(self.target.__name__)


class ProcessLibrary(object):
    _registered_processes = []
    _started_processes = []

    def register(self, target, name=None, if_true=None, event_handler=None, *args, **kwargs):
        rp = RegisteredProcess(target, if_true, name, event_handler=event_handler, *args, **kwargs)
        if rp not in self._registered_processes:
            self._registered_processes.append(rp)
        return True

    def run_all(self, app):
        for rp in self._registered_processes:
            if rp.can_run(app):
                app.logger.debug("Run {0}".format(rp))
                process = rp.run(app)
                self._started_processes.append(process)
        return self._started_processes

    def wait(self):
        def get_process_by_read_connection(con):
            for p in self._registered_processes:
                if p.read_con and p.read_con == con:
                    return p
        if not self._started_processes:
            return
        # Only started processes have a read connection
        readers = [p.read_con for p in self._registered_processes if p.read_con]
        while readers:
            for r in wait(readers):
                try:
                    msg = r.recv()
                except EOFError:
                    readers.remove(r)
                else:
                    rp = get_process_by_read_connection(r)
                    rp.handle_event(msg)


library = ProcessLibrary()
