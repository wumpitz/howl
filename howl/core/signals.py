from blinker import Namespace
howl_signals = Namespace()

gateway_initialised = howl_signals.signal('gateway_initialised')
