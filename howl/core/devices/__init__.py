from flask.ext.admin.contrib.mongoengine import ModelView
from mongoengine import Document, StringField

from howl.core.devices.exceptions import DeviceNotFound, DeviceClassNotFound
from howl.core.documents import admin_app


class BaseDevice(Document):
    """
    The BaseDevice is the core model for creating binding specific
    devices. Create a subclass of this class and overwrite the `fields`
    attribute to add more fields to your device (if needed).
    """

    name = StringField(required=True)
    binding_code = StringField(required=True)

    uri_endpoint = 'device'
    uri_marshal_field_name = 'uri'

    meta = {
        'allow_inheritance': True,
        'collection': 'devices'
    }

    def __repr__(self):
        return '<Device %s/%s %s>' % (self.binding_code, self._cls, self.name)

    def __unicode__(self):
        return repr(self)


_registered_devices = {}


def register(klass, with_admin=True):
    """
    Register a device class
    """
    binding_name = klass.__module__.split('.')[-2]
    if binding_name not in _registered_devices:
        _registered_devices[binding_name] = []
    _registered_devices[binding_name].append(klass)
    if with_admin:
        admin_app.add_view(ModelView(klass, category='Devices'))


def get_devices_for_binding(code):
    devices = []
    device_classes = _registered_devices[code]
    for klass in device_classes:
        d = list(getattr(klass, 'objects'))
        devices += d
    return devices


def get_device_class_for_binding(code, name):
    device_classes = _registered_devices[code]
    for klass in device_classes:
        if klass.__name__ == name:
            return klass
    raise DeviceClassNotFound('DeviceClass "%s" for binding "%s" not found' % (name, code))
