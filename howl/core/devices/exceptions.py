class DeviceNotFound(Exception):
    pass


class DeviceClassNotFound(Exception):
    pass
