import importlib
from howl.core.bindings.exceptions import BindingNotFound
from howl.core.signals import gateway_initialised

bindings = []


class Binding(object):
    """
    A Binding is the interface to one of your home automation tools like Homematic, KNX and many more.
    """

    def __init__(self, code, module, conf):
        self.code = code
        self.name = self.code
        self.module = module
        self.gateway = None
        self._conf = conf

    def setup(self):
        if not hasattr(self.module, 'gateway'):
            raise ImportError('No "gateway" defined for binding %s' % self.name)
        self.gateway = self.module.gateway(conf=self._conf)
        if self.gateway.name:
            self.name = self.gateway.name

        # Call the 'on_initialized' method of the gateway after the setup of a binding
        # Todo use signals
        self.gateway.on_initialized()

    def __str__(self):
        return str(self.name)

    def __unicode__(self):
        return str(self)


class BindingFactory(object):
    """Factory for initialising a binding. Checks for prerequisites, gets configurations
    and initialises a binding finally.
    """

    @classmethod
    def parse_name(cls, module):
        return module.__name__.split('.')[-1]

    @classmethod
    def get_conf_for_binding(cls, conf, name):
        return conf.get(name.upper(), {})

    @classmethod
    def create_binding(cls, module, conf):
        """

        :param module: The imported binding module
        :param conf: The app configuration object
        :return: Created binding
        :rtype: Binding
        """
        name = cls.parse_name(module)
        conf = cls.get_conf_for_binding(conf, name)
        binding = Binding(name, module, conf)
        binding.setup()
        gateway_initialised.send(binding.gateway.__class__, instance=binding.gateway)
        return binding


def _load_binding(conf, module_path):
    """
    Load the binding from the given module path
    :param module_path:
    :return:
    """
    module = importlib.import_module(module_path)
    return BindingFactory.create_binding(module, conf)


def load_bindings(conf, module_list):
    """
    Load and initialise each binding from the given module list
    :param module_list:
    :return:
    """
    for module_path in module_list:
        binding = _load_binding(conf, module_path)
        bindings.append(binding)
    return bindings


def load_devices(module_list):
    for module_path in module_list:
        importlib.import_module('.'.join([module_path, 'devices']))


def get_binding(code):
    for binding in bindings:
        if binding.code == code:
            return binding
    raise BindingNotFound('Binding "%s" not found' % code)
