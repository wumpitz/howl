
class BaseGateway(object):
    """
    A gateway for Howl to the contrib bindings. Howl interacts with all bindings
    through a gateway. Every binding has to implement it's own gateway.
    """
    name = None

    def __init__(self, conf):
        self.conf = conf

    def fetch_item_value(self, item):
        """Gets called to get the real value from the item/device (not from database)"""
        raise NotImplementedError('')

    def set_item_value(self, item, value):
        """Gets called to set the value of an item/device in real (not in database)"""
        raise NotImplementedError('')

    def on_initialized(self):
        """Gets called after the binding was initialized and set up"""
        pass
