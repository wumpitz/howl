from howl.core.bindings import get_binding
from howl.core.items import BaseItem
from howl.core.items import services
from .app import celery
#from flask import current_app


@celery.task
def fetch_value(item_id):
    """Fetch the real value from the item and save it.

    :param item_id: The items id
    :type item_id: str
    :return: real value
    """
    item = BaseItem.objects.get(id=item_id)
    binding = get_binding(item.binding_code)
    # Get the value using the bindings gateway
    value = binding.gateway.fetch_item_value(item)
    services.update_item(item, value)
    return value


def call_fetch_value(item_id):
    #app = current_app._get_current_object()
    fetch_value.delay(item_id)
