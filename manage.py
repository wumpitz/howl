#!/usr/bin/env python

from flask.ext.script import Manager, Shell, Server
from howl.app import create_app
from howl.core.processes.commands.run_subprocesses import Executor

app = create_app()
manager = Manager(app)

# Override default 'runserver' command defaults.
# For further arguments check manage.py runserver -?
manager.add_command('runserver', Server(host='0.0.0.0'))
manager.add_command('shell', Shell())
manager.add_command('run_processes', Executor())

if __name__ == '__main__':
    manager.run()
