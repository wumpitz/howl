
# What is Howl?

Howl is a piece of software for your home automation.

# Planned features

* Support for different protocols and products
* Easy-to-use configuration interface
* Nice user interface for your pleasure
* "Actions" changes the state of a device
* "Rules" to perform a set of actions
* "Profiles" for applying a bunch of rules

# Usage

Start celery:

    celery worker -A celery_worker.celery glevel=info
