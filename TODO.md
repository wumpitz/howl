Persistance:

* Celerybeat für Persistance evaluieren
    * Kann CB aus der App heraus gestartet werden? Ist das eine gute Idee?
    * Neu laden bei Konfigurationsänderungen notwendig
* Wie wird Persistance-Konfiguration abgespeichert?
* Was soll zu Beginn unterstützt werden?
    * OnChange
    * Alle X Minuten
    * Einfach durchreichen bis zu CB?

Items:

* Das Setzen von Values über API ermöglichen
* Value-Anpassung bei OnChange (Homematic teilt Änderung mit)

API:

* Das Setzen von Values bei Items ermöglichen
* Persistierte Daten ausgeben (um diese in Diagrammen verwenden zu können)
